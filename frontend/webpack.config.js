const webpack = require('webpack');
const path = require('path');


var BUILD_DIR = path.resolve(__dirname, 'static');
var SRC_DIR = path.resolve(__dirname, 'src');
console.log(BUILD_DIR, SRC_DIR);
module.exports = {
  entry: SRC_DIR + '/index.js',
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        include: SRC_DIR,
        loader: 'babel-loader',
        query: {
          presets: ['env', 'react'],
          plugins: ['transform-object-rest-spread', 'transform-runtime']
        }
      }
    ]
  },
  output: {
    path: BUILD_DIR,
    filename: 'static/js/bundle.js',
    publicPath: '/'
  },
  devServer: {
    host: '0.0.0.0',
    port: 3000,
    clientLogLevel: 'none',
    disableHostCheck: true
  }
};