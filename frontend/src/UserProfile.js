import React from 'react';
import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';

const User = props => {
  const {
    profile_background_color, profile_background_image_url, profile_image_url, screen_name, name, onRefresh, onLogOut
  } = props;
  return (
    <Paper style={{ display: "flex", padding: 8, backgroundColor: '#' + profile_background_color, width: "100%", position: "relative" }}>
      {profile_background_image_url && <img src={profile_background_image_url} alt="background"/>}
      <div style={{ display: "flex", flexDirection: "row" }}>
        <img src={profile_image_url} alt={screen_name} style={{ borderRadius: "50%" }}/>
        <div style={{ padding: 4 }}>
          <div style={{ fontWeight: "bold" }}>{name}</div>
          <div>@{screen_name}</div>
        </div>
      </div>
      <div style={{ position: "absolute", right: 8 }}>
        <RaisedButton label="Refresh Tweets" primary onClick={onRefresh} />
        {' '}
        <RaisedButton label="Log Out" onClick={onLogOut} />
      </div>
    </Paper>
  );
};

export default User;