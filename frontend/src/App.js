import React, { Component } from 'react';

import { oauthRequest, connect, tweets, logout } from './api';

import UserProfile from './UserProfile';
import Tweet from "./Tweet";

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      signInUrl: "",
      data: {},
      userTimeLine: [],
    };
    this.refreshTweets = this.refreshTweets.bind(this);
    this.logOut = this.logOut.bind(this);
  }

  async componentDidMount() {
    this.setState({ loading: true });
    const oauthParams = {};
    if (location.search && location.search.length > 1) {
      location.search.slice(1).split('&').forEach(value => {
        const pair = value.split('=');
        if (pair.length === 2 && ['oauth_token', 'oauth_verifier'].indexOf(pair[0]) >= 0) {
          oauthParams[pair[0]] = pair[1];
        }
      });
    }
    const { oauth_token, oauth_verifier } = oauthParams;
    if (!oauth_token || !oauth_verifier) {
      const token = window.localStorage.getItem('token');
      if (!token) {
        const url = await oauthRequest();
        this.setState({ signInUrl: url, loading: false  });
      } else {
        const data = JSON.parse(window.localStorage.getItem('data'));
        await tweets();
        const userTimeLine = JSON.parse(window.localStorage.getItem('tweets'));
        this.setState({ signInUrl: "", data, userTimeLine, loading: false  });
      }
    } else {
      await connect(oauth_verifier, oauth_token);
      window.location.replace("/");
    }
  }

  async refreshTweets() {
    this.setState({ loading: true  });
    await tweets();
    const userTimeLine = JSON.parse(window.localStorage.getItem('tweets'));
    this.setState({ userTimeLine, loading: false  });
  }

  async logOut() {
    this.setState({ loading: true });
    logout();
    const url = await oauthRequest();
    this.setState({ signInUrl: url, data: {}, userTimeLine: [], loading: false  });
  }

  render() {
    const { loading, signInUrl, data, userTimeLine } = this.state;
    return (
      <div style={{ display: "flex", flexDirection: "column", alignItems: "center", color: '#' + data.profile_text_color, backgroundColor: '#' + data.profile_background_color, height: "100%" }}>
        {data.id && <UserProfile {...data} onRefresh={this.refreshTweets} onLogOut={this.logOut} />}
        <br />
        {signInUrl && <a href={signInUrl}>
          <img src="assets/sign-in-with-twitter-gray.png" alt="sign in with twitter"/>
        </a>}
        {loading && <div>LOADING</div>}
        <div style={{ display: "flex", flexDirection: "column", alignItems: "center" }}>
          {userTimeLine.map(tweet => <Tweet {...tweet}/>)}
        </div>
      </div>
    );
  }
}

export default App;
