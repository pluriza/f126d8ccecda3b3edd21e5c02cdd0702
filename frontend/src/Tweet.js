import React from 'react';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';

const Tweet = props => {
  const {user: { profile_image_url, screen_name, name }, created_at, text,} = props;
  return (
    <Card style={{ width: 512, marginBottom: 16 }}>
      <CardHeader
        title={name}
        subtitle={`@${screen_name} · ${created_at}`}
        avatar={profile_image_url}
      />
      <CardText>{text}</CardText>
    </Card>
  );
};

export default Tweet;
