const HOST = "http://localhost:8080";

const request = async (method, url, config = {}) => {
  const { headers, data, ...restConfig } = config;

  const response = await fetch(url, {
    ...restConfig,
    method,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json;charset=UTF-8',
      ...headers,
    },
    body: JSON.stringify(data),
  });
  const json = await response.json();
  if (response.ok) {
    return json;
  }

  throw new Error(json.message);
};

export const oauthRequest = async () => {
  const { url, token } = await request('GET', `${HOST}/oauth_request`);
  window.localStorage.setItem('requestToken', token);
  return url;
};

export const connect = async (oauth_verifier, oauth_token) => {
  const token = window.localStorage.getItem('requestToken');
  const config = {
    data: { oauth_verifier, oauth_token },
    headers: { Authorization: `Bearer ${token}` },
  };
  const { data, token: accessToken } = await request('POST', `${HOST}/connect`, config);
  window.localStorage.removeItem('requestToken');
  window.localStorage.setItem('token', accessToken);
  window.localStorage.setItem('data', JSON.stringify(data));
};

export const tweets = async () => {
  const token = window.localStorage.getItem('token');
  const config = {
    headers: { Authorization: `Bearer ${token}` },
  };
  const { data } = await request('GET', `${HOST}/tweets`, config);
  window.localStorage.setItem('tweets', JSON.stringify(data));
  return data;
};

export const logout = () => {
  window.localStorage.removeItem('tweets');
  window.localStorage.removeItem('data');
  window.localStorage.removeItem('token');
};