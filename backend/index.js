const express = require('express');
const bodyParser = require('body-parser');
const oauth = require('oauth');
const passport = require("passport");
const cors = require('cors');
const configureRouter = require('./config/routes');
const configurePassport = require('./config/passport');

const { PORT = 8080 } = process.env;

const app = express();
configurePassport();
app.use(passport.initialize());
app.use(cors({ origin: '*' }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.text());
app.use(bodyParser.json({ type: 'application/json' }));

const router = express.Router();
const twitter = new oauth.OAuth(
  "https://twitter.com/oauth/request_token", "https://twitter.com/oauth/access_token",
  "aGsGKNEK4xxxoX2KvdumICQEJ", "nrQRAxF4AjwCP8bPwJTjZr07GHoUfqBVk4jj44fnIGQEFvoPZE", "1.0A",
  "http://localhost:3000/", "HMAC-SHA1"
);
configureRouter(router, twitter);
app.use("/", router);

module.exports = app.listen(PORT, err => {
  if (err) throw err;
  console.log(`Server listening on port ${PORT}!`);
});