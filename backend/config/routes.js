const { compose } = require("compose-middleware");
const passport = require("passport");
const jwt = require("jwt-simple");

const { JWT_TOKEN, TOKEN_EXPIRATION_TIME } = require('./config');

const createExpirationDate = () => new Date(Date.now() + TOKEN_EXPIRATION_TIME);
const createToken = async data => await jwt.encode({ ...data, expirationDate: createExpirationDate() }, JWT_TOKEN, null, null);

module.exports = (router, twitter) => {
  router.get("/oauth_request", (req, res) => {
    twitter.getOAuthRequestToken(async (error, oauthToken, oauthTokenSecret, results) => {
      if (error) {
        console.log(error);
        return res.status(500).send({ message: "Error getting OAuth request token", error });
      }
      const token = await createToken({ oauthRequestToken: oauthToken, oauthRequestTokenSecret: oauthTokenSecret });
      res.status(200).send({ url: `https://twitter.com/oauth/authorize?oauth_token=${oauthToken}`, token });
    });
  });
  router.post("/connect", compose([
    passport.authenticate("bearer"),
    async (req, res) => {
      const {
        user: { oauthRequestToken, oauthRequestTokenSecret, oauthAccessToken, oauthAccessTokenSecret, ...user },
        body: { oauth_verifier, oauth_token },
      } = req;
      if ((!oauthRequestToken || !oauthRequestTokenSecret) && (oauthAccessToken && oauthAccessTokenSecret)) {
        const token = await createToken({ oauthAccessToken, oauthAccessTokenSecret, ...user });
        return res.status(200).send({ user, token });
      }
      if (oauth_token !== oauthRequestToken) {
        return res.status(500).send({ message: "Error validating OAuth token" });
      }
      twitter.getOAuthAccessToken(oauthRequestToken, oauthRequestTokenSecret, oauth_verifier, (error, oauthAccessToken, oauthAccessTokenSecret, results) => {
        if (error) {
          console.log(error);
          return res.status(500).send({ message: "Error getting OAuth access token", error });
        }
        twitter.get("https://api.twitter.com/1.1/account/verify_credentials.json", oauthAccessToken, oauthAccessTokenSecret, async (error, data, response) => {
          if (error) {
            console.log(error);
            return res.status(500).send({ message: "Error getting user data", error });
          }
          data = JSON.parse(data);
          const token = await createToken({ oauthAccessToken, oauthAccessTokenSecret, ...data });
          res.status(200).send({ data, token });
        });
      });
    },
  ]));
  router.get("/tweets", compose([
    passport.authenticate("bearer"),
    (req, res) => {
      const { user } = req;
      twitter.get(`https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=${user.screen_name}`, user.oauthAccessToken, user.oauthAccessTokenSecret, (error, data, response) => {
        if (error) {
          console.log(error);
          return res.status(500).send({ message: "Error getting user tweets", error });
        }
        res.status(200).send({ data: JSON.parse(data) });
      });
    },
  ]));
  router.post("/disconnect", (req, res) => {
    const { userId: id } = req.session;
    req.session.destroy(error => {
      if (error) {
        console.log(error);
        return res.status(500).send({ message: "Error getting user data", error });
      }
      res.status(200).send({ id });
    });
  });
};