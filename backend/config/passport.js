const passport = require("passport");
const jwt = require("jwt-simple");
const BearerStrategy = require("passport-http-bearer").Strategy;

const { JWT_TOKEN } = require('./config');

module.exports = () => {
  passport.use(new BearerStrategy({ session: false }, async (token, done) => {
    console.log('token', token)
    const decodedToken = jwt.decode(token, JWT_TOKEN, false, 'HS256');
    const expires = decodedToken && new Date(decodedToken.expirationDate);
    if (expires > Date.now()) {
      return done(null, decodedToken);
    } else {
      return done(null, false);
    }
  }));
};

passport.serializeUser((user, done) => done(null, user));

passport.deserializeUser((user, done) => done(null, user));