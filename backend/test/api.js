process.env.NODE_ENV = "test";

const chai = require('chai');
const chaiHttp = require('chai-http');

const backend = require('../index');

chai.should();
chai.use(chaiHttp);

describe("/GET oauth_request", () => {
  it("it should get a response with the Twitter authorisation URL", done => {
    chai.request(backend).get("/oauth_request").end((err, res) => {
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('url');
      console.log("res.body", res.body);
      done();
    });
  });
});

describe("/POST connect", () => {
  it("it should get a response with all the Twitter profile data", done => {
    chai.request(backend).post("/connect").end((err, res) => {
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('message');
      res.body.should.have.property('message').eql("good");
      done();
    });
  });
});

describe("/GET tweets", () => {
  it("it should get a response with a list containing 100 of the most resent tweets", done => {
    chai.request(backend).get("/tweets").end((err, res) => {
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('message');
      res.body.should.have.property('message').eql("good");
      done();
    });
  });
});

describe("/POST disconnect", () => {
  it("it should get a response with the Twitter ID", done => {
    chai.request(backend).post("/connect").end((err, res) => {
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('message');
      res.body.should.have.property('message').eql("good");
      done();
    });
  });
});
