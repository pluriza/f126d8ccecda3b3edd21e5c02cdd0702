## Installation

Using node v9.8.0

Backend
```sh
# From root dir
cd backend && yarn
# or if you don't have yarn
cd backend && npm install
```

Frontend
```sh
# From root dir
cd frontend && yarn
# or if you don't have yarn
cd frontend && npm install
```

## Start

Backend
```sh
# From root dir
cd backend && yarn start
# or if you don't have yarn
cd backend && npm run start
```

Frontend
```sh
# From root dir
cd frontend && yarn start:dev
# or if you don't have yarn
cd frontend && npm run start:dev
```

## Testing

Backend
```sh
# From root dir
cd backend && yarn test
# or if you don't have yarn
cd backend && npm run test
```
